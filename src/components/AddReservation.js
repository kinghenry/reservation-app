import React, { PureComponent } from 'react';
import { TouchableOpacity, View, Platform, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Colors } from '@config';
import { NavigationHelpers } from '@routes/';

type Props = {
  name: string,
  modalName: string,
  modalTitleText: string,
  iconName?: string,
};

const AddReservation = (props: Props) => {
  const addReservation = () => {
    NavigationHelpers.showModal(
      'CreateReservationModal',
      'AddReservationModal',
      'Create Reservation',
      { name: props.name },
    );
  };

  return (
    <View styles={styles.container}>
      <TouchableOpacity onPress={() => addReservation()} activeOpacity={0.8}>
        <Icon name={'plus'} size={30} color={Colors.primary} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: Platform.OS === 'android' ? 30 : 0,
    width: Platform.OS === 'android' ? 60 : 0,
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
});

export default AddReservation;
