import React from 'react';
import { Navigation } from 'react-native-navigation';
import { TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Colors } from '@config/';

type Props = {
  componentId: string,
};

const CloseModalButton = (props: Props) => {
  /** hard coded to close reservation just for this challenge */
  const closeModal = () => {
    Navigation.dismissModal('CreateReservationModal');
  };

  return (
    <View>
      <TouchableOpacity onPress={() => closeModal()} activeOpacity={0.8}>
        <Icon name="close" size={25} color={Colors.primary} />
      </TouchableOpacity>
    </View>
  );
};

export default CloseModalButton;
