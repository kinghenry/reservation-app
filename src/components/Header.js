import {Colors} from '@config';
import type {Node} from 'react';
import Images from '@assets/';
import {HiltonLogo} from '@assets/';
import {Image, Text, StyleSheet, View} from 'react-native';
import React from 'react';

const Header = (): Node => (
  <View>
    <Image
      testID={'header-img'}
      source={Images.hiltonLogo}
      style={styles.background}
      imageStyle={styles.logo}
    />
    <Text style={styles.text}>Login to access the Reservation Manger</Text>
  </View>
);

const styles = StyleSheet.create({
  background: {
    paddingBottom: 40,
    height: 300,
    width: '100%',
    paddingTop: 96,
    paddingHorizontal: 32,
    backgroundColor: Colors.lighter,
  },
  logo: {
    opacity: 0.2,
    overflow: 'visible',
    resizeMode: 'cover',
  },
  text: {
    fontSize: 20,
    fontWeight: '600',
    textAlign: 'center',
    color: Colors.black,
  },
});

export default Header;
