import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { ListItem } from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import { Colors } from '@config/';

type Props = {
  label: String,
  initialDate?: string,
  setDate: Function,
  date: String,
};

const ReservationDatePicker = (props: Props) => {
  const datePicker = (
    <DatePicker
      style={{ width: 200 }}
      date={props.date}
      mode="date"
      placeholder="Select date"
      format="MM-DD-YYYY"
      confirmBtnText="Confirm"
      cancelBtnText="Cancel"
      iconComponent={
        <FAIcon
          name="calendar"
          size={22}
          style={{ marginLeft: 10 }}
          color={Colors.primary}
        />
      }
      customStyles={{
        dateInput: {
          marginRight: 10,
        },
        dateInput: {
          borderWidth: 0,
          alignItems: 'flex-end',
        },
        placeholderText: {
          fontSize: 16,
        },
        dateText: {
          fontSize: 16,
          color: Colors.primary,
        },
        btnTextConfirm: {
          height: 20,
          color: Colors.primary,
        },
        btnTextCancel: {
          height: 20,
        },
      }}
      onDateChange={selectedDate => props.setDate(selectedDate)}
    />
  );
  return (
    <ListItem
      title={props.label}
      rightElement={datePicker}
      bottomDivider
      chevron
      containerStyle={styles.listItem}
    />
  );
};

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: Colors.white,
  },
});

export default ReservationDatePicker;
