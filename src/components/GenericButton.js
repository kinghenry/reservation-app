import React, { Component } from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import { Colors } from '@config';
import { Icon } from 'react-native-elements';

type Props = {
  disabled: Boolean,
  onPress: Function,
  leftIcon?: Icon,
  rightIcon?: Icon,
  title: String,
  width: Number,
  loading?: Boolean,
  textColor: String,
  title?: String,
  style?: Object,
};

const GenericButton = (props: Props) => {
  const {
    color,
    loading,
    style,
    inverted,
    disabled,
    leftIcon,
    rightIcon,
    title,
    width,
  } = props;
  const disabledColor = '#999999';
  /*
            button color is assigned a value based on the disabled prop:
            if disabled it will be the disabled color defined above,
            else the color passed in as props
          */
  let buttonColor = disabled ? disabledColor : color;
  const buttonWidth = width ? width : `80%`;
  /**
   * `_onPress` is our internal implementation local to this component,
   * prefixed with an underscore so as to not confuse or clash with
   * native `onPress`
   *
   * we pass in `disabled` and the `onPress` function from the parent container from props
   * the `disabled` property is used by this generic button component class:
   *  -to prevent clicks from generating actions
   *  - to conditionally style the button, either to enabled state color or disabled state color
   * @param args
   * args is an anonymous value reference to be passed when it is called
   * @private
   */
  const _onPress = args => {
    /**
     * if disabled, continue on press with whatever args the caller has provided
     * otherwise, do nothing (prevent onPress event from firing its connected action)
     */
    if (!disabled) props.onPress(args);
  };

  const getInnerView = () => {
    if (loading) {
      return <ActivityIndicator size="small" color={Colors.primary} />;
    } else {
      return (
        <Text style={[styles.buttonText, { color: '#e8e5df' }]}>{title}</Text>
      );
    }
  };

  return (
    <TouchableOpacity
      onPress={_onPress}
      /**
       *
       * applying styles in line so we can:
       *   - reference our local function variable, `buttonColor`
       *   - pass in styles as an array, they will be applied left to right
       */
      style={[
        styles.buttonView,
        { backgroundColor: buttonColor, width: buttonWidth, ...style },
      ]}>
      {getInnerView()}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  buttonView: {
    alignItems: 'center',
    height: 50,
    borderWidth: 0.5,
    borderRadius: 5,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 22,
    fontFamily: 'Avenir-Heavy',
    letterSpacing: 1.39,
    lineHeight: 30,
  },
});

GenericButton.defaultProps = {
  textColor: '#FFFFFF',
  color: Colors.primary,
  onPress: () => '',
  title: 'Next',
  inverted: false,
  disabled: false,
  style: {},
};

export default GenericButton;
