import React, {useState} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {Input} from 'react-native-elements';
import * as Animatable from 'react-native-animatable';

type Props = {
  name: string,
};

const GenericInput = (props: Props) => {
  const {label, autoFocus, input, error} = props;
  const [name, setName] = useState('');
  const [isSecure, setIsSecure] = useState(name === 'password');
  const [autoCapitalize, setAutoCapitalize] = useState('none');
  const [keyboardType, setKeyboardType] = useState('default');
  const [isFocused, setIsFocused] = useState(false);
  if (name === 'email') setKeyboardType('email-address');

  const onFocus = e => {
    setIsFocused(true);
  };

  const onBlur = e => {
    setIsFocused(false);
  };

  return (
    <View style={styles.mainContainer}>
      <Input
        testID={name}
        accessible={true}
        accessibilityLabel={`${label}-input-field`}
        autoFocus={autoFocus}
        onChangeText={text => props.onChangeText(text)}
        onBlur={onBlur}
        onFocus={onFocus}
        value={props.value}
        placeholder={label}
        spellCheck={false}
        keyboardType={keyboardType}
        autoCorrect={false}
        autoCapitalize={autoCapitalize}
        secureTextEntry={isSecure}
        clearButtonMode="while-editing"
        offset={0}
        underlineColorAndroid="transparent"
        textStyle={styles.elementsInput}
        textProps={{
          ...input,
          inputContainerStyle: [styles.input],

          onFocus: onFocus,
          onBlur: onBlur,
        }}
      />
      {!!error && (
        <Animatable.View useNativeDriver={true} animation="fadeIn">
          <Text style={styles.errorText}> {error} </Text>
        </Animatable.View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 50,
    marginTop: 20,
    borderWidth: 0,
  },
  errorText: {
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
  elementsInput: {
    height: 60,
    fontSize: 20,
    marginBottom: -5,
  },
});

export default GenericInput;
