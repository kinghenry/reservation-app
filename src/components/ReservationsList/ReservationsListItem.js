import React from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Colors } from '@config/';
import Images from '@assets/';
type Props = {
  hotelName: String,
  arrivalDate: Date,
  departureDate: Date,
};

const ReservationsListItem = (props: Props) => {
  const { hotelName, arrivalDate, departureDate } = props;
  return (
    <TouchableOpacity
      style={styles.reservationContainer}
      activeOpacity={0.7}
      onPress={() => console.log(props)}
      accessible={false}>
      <View
        accessible={true}
        accessibilityLabel={`${hotelName}${arrivalDate}`}
        testID={`${hotelName}${arrivalDate}`}
        style={styles.reservationPlusImg}>
        <View style={styles.reservationDetails}>
          <Text style={styles.hotelNameText}>{hotelName}</Text>
          <View style={styles.imgAndDates}>
            <Image style={styles.hotelImage} source={Images.randomHotel3} />
            <View style={styles.dates}>
              <Text style={styles.dateRangeText}>Arrival: {arrivalDate}</Text>
              <Text style={styles.dateRangeText}>
                Departure: {departureDate}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  reservationContainer: {
    backgroundColor: 'white',
    borderColor: '#d3d3d3',
    borderWidth: 1,
    padding: 10,
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 5,
    ...Platform.select({
      android: {
        elevation: 1,
      },
      default: {
        shadowColor: 'rgba(0,0,0, .2)',
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 2,
        shadowRadius: 2,
      },
    }),
  },
  hotelImage: {
    width: 90,
    height: 100,
    resizeMode: 'contain',
    borderRadius: 10,
    alignSelf: 'center',
    marginRight: 5,
  },
  reservationPlusImg: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center',
    height: 130,
  },
  reservationDetails: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'flex-start',
  },
  imgAndDates: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dates: {
    flexDirection: 'column',
    alignSelf: 'center',
  },
  hotelNameText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    alignSelf: 'flex-start',
  },
});

export { ReservationsListItem };
