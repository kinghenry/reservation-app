import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const NoReservations = () => (
  <View style={styles.noReservations}>
    <Text style={styles.cellHeaderText}>No Reservations found</Text>
    <Text style={styles.cellBodyText}>Click the '+' to add a reservation</Text>
  </View>
);

const styles = StyleSheet.create({
  noReservations: {
    height: 100,
    padding: 10,
    margin: 10,
  },
  cellHeaderText: {
    alignSelf: 'center',
    color: 'grey',
    fontSize: 20,
    marginBottom: 5,
  },
  cellBodyText: {
    alignSelf: 'center',
    color: 'grey',
    fontSize: 16,
    textAlign: 'center',
  },
});

export {NoReservations};
