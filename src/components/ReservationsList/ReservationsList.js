import React from 'react';
import {
  Text,
  ActivityIndicator,
  View,
  FlatList,
  StyleSheet,
} from 'react-native';
import { NoReservations, ReservationsListItem } from './';

type Props = {
  reservations: Array<Object>,
  refreshing: Boolean,
  onRefresh: Function,
};

const ReservationsList = (props: Props) => {
  const { reservations } = props;

  const _renderRowData = item => {
    const { item: reservation } = item;
    return (
      <ReservationsListItem
        hotelName={reservation.hotelName}
        arrivalDate={reservation.arrivalDate}
        departureDate={reservation.departureDate}
      />
    );
  };

  return (
    <FlatList
      data={reservations}
      enableEmptySections={true}
      keyExtractor={item => item.id}
      showsVerticalScrollIndicator={false}
      style={styles.reservationsList}
      renderItem={({ item }) => _renderRowData({ item })}
      getItemLayout={(data, index) => ({
        length: 20,
        offset: 20 * index,
        index,
      })}
      refreshing={props.refreshing}
      onRefresh={props.onRefresh}
      ListEmptyComponent={<NoReservations />}
    />
  );
};

const styles = StyleSheet.create({
  reservationsList: {
    borderTopColor: 'lightgrey',
    borderTopWidth: 1,
    padding: 10,
    marginTop: 10,
  },
});

export { ReservationsList };
