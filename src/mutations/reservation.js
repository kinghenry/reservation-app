import gql from 'graphql-tag';

export const CREATE_RESERVATION_MUTATION = gql`
  mutation CREATE_RESERVATION_MUTATION(
    $name: String!
    $hotelName: String!
    $arrivalDate: String!
    $departureDate: String!
  ) {
    createReservation(
      data: {
        name: $name
        hotelName: $hotelName
        arrivalDate: $arrivalDate
        departureDate: $departureDate
      }
    ) {
      id
      arrivalDate
      departureDate
    }
  }
`;
