import gql from 'graphql-tag';

export const MY_RESERVATIONS_QUERY = gql`
  query getMyReservations($name: String) {
    reservations(where: { name: $name }) {
      id
      name
      hotelName
      arrivalDate
      departureDate
    }
  }
`;
