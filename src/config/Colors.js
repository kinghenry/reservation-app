'use strict';

export default {
  primary: 'rgb(16, 76, 151)',
  white: '#FFF',
  lighter: '#F3F3F3',
  light: '#DAE1E7',
  dark: '#444',
  black: '#000',
};
