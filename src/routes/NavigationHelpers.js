import { Navigation } from 'react-native-navigation';
import { Colors } from '@config';

class NavigationHelpers {
  /**
   * this function is used to scaffold the login page
   * this is kept as a navigation stack for this simple app
   * because the typical login/register/forgot password flow
   * is best implemented as a navigation stack,
   * pushing and popping the respective screens
   * as the user needs them,
   * we can add more children to the array of components
   * in this stack when we need more screens in this flow
   */
  static restartApp = (props = {}) => {
    Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: 'LoginScreen',
                passProps: props,
                options: {
                  topBar: {
                    title: {
                      text: 'Login',
                    },
                    leftButtonColor: Colors.primary,
                  },
                },
              },
            },
          ],
        },
      },
    });
  };

  /**
   * @flow
   * presents a view controller in a modal fashion
   * on iOS this animates up from the bottom
   * on android this doesn't change much about the animation,
   * however it will render with a close icon
   * in the left upper corner rather than a back arrow
   * @param modalName
   *  - the name of the screen we want to present modally
   * @param modalTitleText
   *  - the title we want to display on the modal
   * @param props
   *  - additional props we want to pass to this screen
   *  - defaults to an empty object
   */
  static showModal = (
    modalId: String,
    modalName: string,
    modalTitleText: string,
    props?: Object = {},
  ) => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              id: modalId,
              name: modalName,
              passProps: props,
              options: {
                topBar: {
                  title: {
                    text: modalTitleText,
                  },
                  statusBar: {
                    backgroundColor: Colors.primary,
                  },
                  leftButtons: [
                    {
                      id: 'close',
                      component: { name: 'CloseModalButton', passProps: props },
                    },
                  ],
                },
              },
            },
          },
        ],
      },
    });
  };

  // /**
  //  * @flow
  //  *  pushes a component registered with RNN
  //  *  onto the current navigation stack
  //  * @param componentId
  //  *  - the componentID of the screen we are navigating from
  //  * @param componentName
  //  *  - the name of the component we are pushing onto the stack
  //  * @param title
  //  *  - the title to render on the next screen
  //  * @param props
  //  *  - additional props we want to pass to the next screen
  //  *  - defaults to an empty object
  //  */
  static pushComponent = (
    componentId: string,
    componentName: string,
    title: string,
    props?: Object = {},
    rightButtons: Array<Object> = [],
  ) => {
    Navigation.push(componentId, {
      component: {
        name: componentName,
        passProps: props,
        options: {
          topBar: {
            title: {
              text: title,
            },
            statusBar: {
              backgroundColor: Colors.primary,
            },
            rightButtons,
          },
        },
      },
    });
  };
}

export { NavigationHelpers };
