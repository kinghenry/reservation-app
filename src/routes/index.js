import React from 'react';
import { Navigation } from 'react-native-navigation';
import { ApolloClient, HttpLink, InMemoryCache } from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import fetch from 'unfetch';
//Screens
import Login from '@screens/Login';
import Reservations from '@screens/Reservations';
import AddReservationModal from '@screens/AddReservation';
import { AddReservation, CloseModalButton } from '@components';

const client = new ApolloClient({
  link: new HttpLink({
    uri:
      'https://us1.prisma.sh/public-luckox-377/reservation-graphql-backend/dev',
    fetch: fetch,
  }),
  cache: new InMemoryCache(),
});

const withApolloProvider = Component => {
  return props => (
    <ApolloProvider client={client}>
      <Component {...props} />
    </ApolloProvider>
  );
};

export function registerScreens() {
  Navigation.registerComponent(
    'LoginScreen',
    () => withApolloProvider(Login),
    () => Login,
  );
  Navigation.registerComponent(
    'ReservationsScreen',
    () => withApolloProvider(Reservations),
    () => Reservations,
  );

  Navigation.registerComponent(
    'AddReservationModal',
    () => withApolloProvider(AddReservationModal),
    () => AddReservationModal,
  );

  Navigation.registerComponent(
    'AddReservation',
    () => withApolloProvider(AddReservation),
    () => AddReservation,
  );
  Navigation.registerComponent(
    'CloseModalButton',
    () => withApolloProvider(CloseModalButton),
    () => CloseModalButton,
  );
}

export * from './NavigationHelpers';
