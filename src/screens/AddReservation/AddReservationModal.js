import React, { useState, useEffect } from 'react';
import {
  Alert,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Text,
  View,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { useMutation } from '@apollo/react-hooks';
import { Colors } from '@config';
import { CREATE_RESERVATION_MUTATION } from '@mutations';
import {
  GenericButton,
  GenericInput,
  ReservationDatePicker,
} from '@components/';

type Props = {
  name?: String,
  hotelName?: String,
  arrivalDate?: String,
  departureDate?: String,
};

const AddReservationModal = (props: Props) => {
  const [name, setName] = useState(props.name);
  const [hotelName, setHotelName] = useState(props.hotelName);
  const [arrivalDate, setArrivalDate] = useState(props.arrivalDate);
  const [departureDate, setDepartureDate] = useState(props.departureDate);
  const [createReservationMutation, { data, loading, error }] = useMutation(
    CREATE_RESERVATION_MUTATION,
    {
      onCompleted({ createReservationMutation }) {
        Alert.alert('Success', 'Reservation was added', [
          {
            text: 'OK',
            onPress: () => Navigation.dismissModal('CreateReservationModal'),
          },
        ]);
      },
      onError({ createReservationMutation }) {
        Alert.alert('Error', 'Error creating reservation, please try again', [
          {
            text: 'OK',
          },
        ]);
      },
    },
  );

  const createReservation = () => {
    createReservationMutation({
      variables: { name, hotelName, arrivalDate, departureDate },
    });
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.createReservationInputs}>
        <GenericInput label={'Name'} value={name} onChangeText={setName} />
        <GenericInput
          label={'Hotel Name'}
          value={hotelName}
          onChangeText={setHotelName}
        />
      </View>
      <View style={styles.createReservationDates}>
        <ReservationDatePicker
          label="Arrival Date"
          date={arrivalDate}
          setDate={setArrivalDate}
        />
        <ReservationDatePicker
          label="Departure Date"
          date={departureDate}
          setDate={setDepartureDate}
        />
      </View>
      <GenericButton
        loading={loading}
        style={styles.button}
        title={'Create Reservation'}
        onPress={createReservation}
      />
    </ScrollView>
  );
};

AddReservationModal.defaultProps = {
  name: '',
  hotelName: '',
  arrivalDate: '',
  departureDate: '',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.light,
  },
  headerText: {
    fontFamily: 'Avenir Heavy',
    fontSize: 18,
    textAlign: 'center',
  },
  createReservationInputs: {
    flexDirection: 'column',
    height: 100,
    marginTop: 20,
    justifyContent: 'space-between',
  },
  createReservationDates: {
    flexDirection: 'column',
    marginTop: 20,
  },
  button: {
    marginTop: 20,
  },
});

export { AddReservationModal };
