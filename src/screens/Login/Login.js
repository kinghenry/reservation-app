import React, { useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { Colors } from '@config';
import {
  GenericButton,
  GenericInput,
  Header,
  AddReservation,
} from '@components/';
import { NavigationHelpers } from '@routes/';

const Login = (props: Props) => {
  const [name, setName] = useState('');
  const disabled = name.length < 3;
  const error = disabled && 'Name must be at least 3 characters';

  const goToReservations = () => {
    NavigationHelpers.pushComponent(
      props.componentId,
      'ReservationsScreen',
      'Reservations',
      { name },
      [
        {
          id: 'addReservation',
          component: { name: 'AddReservation', passProps: { name } },
        },
      ],
    );
  };
  return (
    <>
      <StatusBar backgroundColor={Colors.primary} />
      <SafeAreaView style={styles.container}>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <Header />
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <GenericInput
                error={error}
                label="name"
                onChangeText={setName}
                value={name}
              />
              <GenericButton
                disabled={disabled}
                title="View Reservations"
                height={100}
                onPress={goToReservations}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f3f3f3',
    flexDirection: 'column',
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  body: {
    backgroundColor: Colors.lighter,
    flex: 1,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: 150,
  },
});

export { Login };
