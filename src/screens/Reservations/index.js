import { graphql } from 'react-apollo';
import { Reservations } from './Reservations';
import { MY_RESERVATIONS_QUERY } from '@queries';

export default graphql(MY_RESERVATIONS_QUERY)(Reservations);
