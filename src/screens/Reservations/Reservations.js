import React, { useState, useEffect } from 'react';
import { SafeAreaView, StyleSheet, ScrollView, Text, View } from 'react-native';
import { Colors } from '@config';
import { isEmpty } from 'lodash';
var moment = require('moment');
import { ReservationsList } from '@components/';

type Props = {
  name: String,
};

const Reservations = (props: Props) => {
  const { reservations } = props.data;
  const [myReservations, setReservations] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  /**
   * filter out invalid dates that come back from graphql query
   * set them into our local state to be passed down to flat list
   */
  useEffect(() => {
    if (!isEmpty(reservations)) {
      const filteredRes = reservations.filter(reservation => {
        return (
          moment(reservation.arrivalDate).isValid() &&
          moment(reservation.departureDate).isValid()
        );
      });
      setReservations(filteredRes);
    }
  }, [reservations]);

  /**
   * apollo refetch binding
   * to flatlist onRefresh action
   */
  const refetchReservations = async () => {
    setRefreshing(true);
    const {
      data: { reservations },
    } = await props.data.refetch({ variables: { name: props.name } });
    setRefreshing(false);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.numReservations}>
        {myReservations.length} Reservations
      </Text>
      <ReservationsList
        refreshing={refreshing}
        onRefresh={refetchReservations}
        reservations={myReservations}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.light,
  },
  calendarContainer: {
    padding: 10,
  },
  numReservations: {
    marginTop: 10,
    textAlign: 'center',
  },
});

export { Reservations };
