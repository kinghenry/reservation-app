/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import { Navigation } from 'react-native-navigation';
import { Colors } from '@config/';
import { registerScreens } from '@routes';

import { NavigationHelpers } from '@routes';

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setDefaultOptions({
    topBar: {
      backButton: {
        color: Colors.primary,
      },
      visible: true,
      hideOnScroll: false,
      drawBehind: false,
    },
  });
  registerScreens();
  NavigationHelpers.restartApp();
});
