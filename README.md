# Hilton Reservation App	
To run on iOS:

1) `yarn install`
2) `cd ios && pod install`
3) `cd ..`

You can then open the .xcworkspace file located in the `ios/` folder and run this on the emulator of your choice


## The task
The task assigned to me was: 
“Create at least 2 screens: 1 for listing the existing reservations and 1 for adding a new reservation.”

## The Product

My approach to this problem started by exploring the data in the Prisma backend that was provided to me. I noticed there was a decent amount of data and some users with reservations. I envisioned an application where clients could create and manage their own reservations, and Hilton employees could login and view all and modify existing reservations for customers. 

### High Level Overview 

I implemented a “login” screen where a user can type their full name `firstName lastName` as it appears in Prisma and this is used in a graphQL query to retrieve all of their own reservations. If there was more time or this was a full project, I would have added an admin panel to view and manage all reservations. 

Their “dashboard” shows a custom React Native Flat List component, with a card style layout for each list item. They can add a new reservation by hitting the ‘+’ button in the upper right corner, which presents a modal form to create a new reservation, with their name pre-populated from props and enabling them to enter a hotel name and use the DatePicker components to select arrival and departure dates.

After a successful reservation is created they are presented a success dialog and brought back to the ReservationsList where they can pull to refresh which will initiate an Apollo refetch and add the new reservation to their list. I would like for this to utilize graphQL subscriptions so they don’t have to manually initiate this refetch action. I can implement so this refetch is automated, but I think from a users perspective it is also good to have a pull to refresh action available to them so that they know what exactly is happening with the application. 

### Implementation Specifics

#### Flow versus TypeScript

I am a TypeScript fan and would definitely advise using it in projects with multiple developers working together, as the strong typing prevents run time errors from being found by users and favors compile time errors for the developers to find the errors in VSCode or whatever their IDE of choice may be. 

However, I decided to use Flow types for this project for the sake of rapid development and since I was working on this alone. Flow still provides me the flexibility of annotating the argument and return types of my functions so that when I call them in other places my IDE can tell me how to call this function so I utilize it properly and avoid run time errors. It is also pre-bundled with react native out of the box and required no additional set up on my end. 

#### Dumb Down the .JSX

Since the advent of React Hooks, classes are out of favor versus Functional Components. I am fully on board with the move toward hooks, as `useState` and `useEffect` provide a much cleaner and readable codebase over lifecycle methods and complicated `componentDidUpdate()` logic to push new data to child components. I made sure to not use Class components anywhere throughout this demo application. 

I also wrapped bare react native components, such as `<TextInput`, `<Button/>` and `<FlatList/>` in wrapper components for a few reasons. One being of course custom styling, we can apply `StyleSheet` properties and custom functions in render props for flat lists to render beautiful custom UI elements. This is an obvious benefit, but the side effect that helps speed up development is that we reduce the available props exposed to other developers and thereby reduce the propensity for errors. There are a vast array of options available as props as is visible in the `<GenericInput/>`, `<GenericButton/>` and `<ReservationsList/>` components. Developers of course are still able to modify these components as they see fit. They can always peruse the React Native documentation to learn about new properties and try to optimize the code even further. But the components we want to reuse throughout our application should require less configuration so they can be re-used easily. Simplicity inspires greater modularity.

#### Apollo/GraphQL

I provided two different methods of binding Apollo/GraphQL queries and mutations to screen files. IN the `AddReservationModal` screen, I used the new `useMutation` hook to keep the logic inside the component itself. In the `Reservations` screen I opted to keep the older curried style of wrapping the `Reservations` component with the results of `MY_RESERVATIONS_QUERY`. This is more akin to the old redux style of `mapStateToProps` or `mapDispatchToProps`. There are pros and cons of each approach. The latter can utilize the `compose` style of passing down several query results to the child component if it requires a lot of data from GQL. The former can keep the logic within the component file as it needs to manipulate and render it. 

To fulfill the requirements of this challenge, I did not implement optimistic UI updates or work much with caching but Apollo provides a great toolset to enable this to be easily added as we need it.

## Code Structure
* ios
	* contains the iOS code, must `cd` here to run cocoa pods install
* android
	* contains the android code, android studio uses this folder as the root to run grade builds
* src
	* assets
		* for storage of static assets, in this case just images, could also be used for custom fonts etc
	* components
		* for components to be used/reused in various screens throughout the entire application
		* some components are grouped together if they are self contained, such as the `<ReservationsList/>` and `<ReservationsListItem/>`
		* as the components undergo further refactoring through the collaborative efforts with other developers, we can abstract these elements into even more functional components that can be exposed at the root level of `@components` to be used in various locations by passing down props or utilizing HOCs_Hooks to customize style_behavior as neccesary
	* config
		* configuration/Theming
		* for this application a simple `Colors.js` object was exposed, but we can add more things as application needs/business logic requires
		* I would vote against keeping server_deployment config files here, as we should store things like the GQL endpoints and other things into .env files, so we can easily switch between QA_Staging/Production environments as neccesary
	* mutations
		* Location to keep graphql mutation functions
		* exposed under a single object, so component developers can simply `import { DESIRED_MUTATION } from “@mutations”` rather than needing to access through each file where the function is exposed
	* queries
		* exposed in the same manner as mutations, contains all the graphQL queries we wish to reuse throughout the application
	* routes
		* contains the scaffolding for react-native-navigation, its binding with Apollo, and Helper functions under `NavigationHelpers.js`.
	* screens
		* root for all the screens which are composed of other React/React-Native components from the `@components` folder
	* utils
		* although unused in this project, provides a location for utility functions, such as regex functions for input validation, date parsing etc
* jsconfig.json
	* to permit named imports for folders, such as `@components/ComponentFile`
	* enables easier legibility and less error prone than relative file path references, i.e. `../../`
* prettierrc.js/eslintrc.js
	* for maintaining code legibility standards
	* the RNC template for eslint suggests best practices, and the prettier ensures formatting
	* VSCode plugin enables format on save using the prettier file
## Architectural Decisions
I find react-native-navigation to be a good navigational platform since it uses native UI components (iOS/Android) internally. This results in much more snappy transitions between screens and a more native look and feel for iOS and Android applications. Its code base itself is rather verbose, and therefore can be rather error prone when initializing complicated tabbed or stack navigational layouts, but this can be alleviated by creating helper functions to be reused by other developers who simply want to develop components or screens. 

I utilized react-native-elements to speed up creating a nice looking input field and `<ListItem/>` , but would vote in the future to create entirely custom `<TextInput/>`  and `<ListItem/>` fields so we could remove the dependence on this library just for a few components. 

The other components are created from raw `<View/>` , `<TouchableOpacity/>` , `<FlatList/>` and other elements. Flexbox with `StyleSheet` enables us to create responsive, flexible and appealing UI elements without the need to depend on external libraries or frameworks. 

### Caveats/Challenges

*Though initially a challenge, I had to upgrade my codebase on a much larger project at my current job to androidX/Jetifier/ RN 0.61.x, so I started here as a base because it was a much simpler setup. So now this builds and works on Android in Android Studio. Thanks again!*

~~I created the application with the latest React-Native, which is version 0.61 as of this writing. This was a risky decision as some libraries are not caught up with the changes on the Android side of things. Since 0.60 and greater we must upgrade the android settings to use AndroidX and Jetifier, and this led to problems building the Android app with Gradle as the build settings for React-Native-Navigation conflicted with the updated Hermes/AndroidX configuration that react-native packaged in the `/android` folder.~~

~~0.61 provided a much better hot reloading experience however, and I am confident that with a day of more dedicated investment in the gradle /Android X and Jetifier settings I could get the application to build successfully. I have run into many more issues in the past by remaining on old versions of React-Native just to keep pieces in tact. This is never a viable long term solution to software maintenance, so although the Android build is temporarily delayed, it is still a better option than using React-Native version 0.59.~~

I’ve purposely overlooked some  UI considerations entirely considering iOS as the only deployment target for the time being. I would have to add a `<BackHandler/>` hook/custom component for android, to cooperate with its hardware back button. I would also vote to utilize a Floating Action Button rather than the ‘+’ button in the nav bar, so we can follow both Apple’s Human Interface Guidelines as well as Google’s Material Design principles for each respective platform. 

Jest_Enzyme tests have not been written but the settings have been updated to allow them to be added. I prioritized feature development_styling over testing for the sake of this challenge but in a real collaborative development environment I would push for a Test-Driven Development approach so all components can be well thought and properly tested so we can release with confidence. I would actually vote to go a step further, and push developers to write Automated UI tests in Swift_Objective-C (XCUITest) and Java_Kotlin (Espresso). This is why the majority of my components have `testID`s and `accessibilityLabel`s. These props are exposed as test Identifiers on iOS and Android views and these native tools can run automated tests with them. These tests can be used with [fastlane](https://fastlane.tools/) to automate screenshot generation. I have effectively been able to use these tests to save myself hours upon hours of painstaking time to generate screenshots for various device types (iPhone 6,7,8,X, XS Max, iPad, iPad Pro, Android phones and tablets, etc..). 

I am proud of the code I have provided here, and I hope this gives a good presentation of my knowledge, experience and wisdom in mobile development with React Native and native iOS and Android development.

Thank you for this opportunity!

-Srihari Rao